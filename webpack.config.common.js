const path = require("path")
const TSLintPlugin = require("tslint-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const dir = (...paths) => {
  return path.join(__dirname, ...paths)
}

module.exports = {
  context: __dirname,
  target: "web",
  resolve: {
    extensions: [ ".ts", ".tsx", ".js", ".less" ],
    modules: [ dir("src"), dir("node_modules") ]
  },
  entry: [ dir("src", "index.tsx") ],
  output: { filename: "index.js" },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          { loader: "cache-loader" },
          { loader: "ts-loader" }
        ],
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: "css-hot-loader"
          },
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true
            }
          },
          {
            loader: "less-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new TSLintPlugin({
      files: ["./src/**/*.ts", "./src/**/*.tsx"],
      warningsAsError: true,
      silent: false,
    })
  ]
}
