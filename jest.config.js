module.exports = {
  preset: "ts-jest/presets/js-with-babel",
  testEnvironment: "node",
  rootDir: ".",
  roots: ["<rootDir>/src/"],
  modulePaths: ["<rootDir>/src"],
  verbose: true,
};
