const common = require("./webpack.config.common");
const config = {
  ...common,
  mode: "development",
  devtool: "inline-source-map"
};

module.exports = config;
