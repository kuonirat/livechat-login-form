import {IPayloadAction} from "actions";
import {IInjector} from "di";
import {IFormAction} from "forms/actions";
import {clearFlash, setFlash} from "modules/flash/actions";
import {setUser, SIGN_IN, SIGN_OUT} from "modules/user/actions";
import {ICredentials} from "modules/user/ICredentials";
import {IUser} from "modules/user/IUser";
import {Either} from "monet";
import {all, getContext, put, takeLatest} from "redux-saga/effects";

export interface ISagaContext {
  injector: IInjector;
}

const signIn: (
  action: IPayloadAction<ICredentials> & IFormAction<ICredentials>
) => IterableIterator<any> = function*(action: IPayloadAction<ICredentials> & IFormAction<ICredentials>): IterableIterator<any> {
  try {
    const injector: IInjector = yield getContext("injector");
    yield put(clearFlash());
    const signInResult: Either<string, IUser> = yield injector.userDAO.signIn(action.payload);
    action.actions.setSubmitting(false);

    yield signInResult.cata(
      function*(error: string): IterableIterator<any> {
        yield put(setFlash({ message: error, level: "error" }));
      },
      function*(user: IUser): IterableIterator<any> {
        yield put(setFlash({ message: "login successful", level: "success" }));
        yield put(setUser(user));
      }
    );
  } catch (error) {
    action.actions.setSubmitting(false);
    yield put(setFlash({ message: error.message, level: "error" }));
  }
};

const signOut: () => IterableIterator<any> = function*(): IterableIterator<any> {
  yield put(clearFlash());
};

const watchSignIn: () => IterableIterator<any> = function*(): IterableIterator<any> {
  yield takeLatest(SIGN_IN, signIn);
};

const watchSignOut: () => IterableIterator<any> = function*(): IterableIterator<any> {
  yield takeLatest(SIGN_OUT, signOut);
};

export const rootSaga: () => IterableIterator<any> = function*(): IterableIterator<any> {
  yield all([
    watchSignIn(),
    watchSignOut()
  ]);
};
