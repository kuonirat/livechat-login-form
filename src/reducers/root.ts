import {flash} from "modules/flash/reducers";
import {user} from "modules/user/reducers";
import {combineReducers, Reducer} from "redux";
import {IMainState} from "state";

export const rootReducer: Reducer<IMainState> = combineReducers<IMainState>({
  flash,
  user
});
