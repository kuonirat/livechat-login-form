import {Maybe} from "monet";
import {Reducer} from "redux";

import {mkDataReducer} from "../../reducers/mkDataReducer";
import {CLEAR_FLASH, SET_FLASH} from "./actions";
import {IFlash} from "./IFlash";

export const flash: Reducer<Maybe<IFlash>> = mkDataReducer<IFlash>([SET_FLASH], [CLEAR_FLASH], { message: "login form example", level: "info" });
