import {findSafe} from "collections";
import {Either, Left, Right} from "monet";

import {ICredentials} from "./ICredentials";
import {IUser} from "./IUser";
import {IUserDAO} from "./IUserDAO";

const salt: string = "secret";
const hash: (s: string) => string = (s) => `${s}${salt}`; // just a mock hashing function

interface IDBEntry {
  user: IUser;
  password: string;
}

const db: IDBEntry[] = [
  {
    user: {
      email: "test@test.pl",
      firstName: "Billy",
      lastName: "The Fridge"
    },
    password: hash("Password1")
  }
];

export class UserDemoDAO implements IUserDAO {
  public signIn(credentials: ICredentials): Promise<Either<string, IUser>> {
    return new Promise(((resolve) => {
      resolve(
        findSafe(db, (entry) => entry.user.email === credentials.email && entry.password === hash(credentials.password))
        .map((entry) => entry.user)
        .cata<Either<string, IUser>>(() => Left("invalid email or password"), Right));
    }));
  }
}
