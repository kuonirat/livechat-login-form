import {Either} from "monet";

import {ICredentials} from "./ICredentials";
import {IUser} from "./IUser";

export interface IUserDAO {
  signIn(credentials: ICredentials): Promise<Either<string, IUser>>;
}
