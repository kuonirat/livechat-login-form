import {ActionCreator, actionCreator, IPayloadAction, payloadActionCreator} from "actions";
import {FormikActions} from "formik";
import {IFormAction} from "forms/actions";

import {ICredentials} from "./ICredentials";
import {IUser} from "./IUser";

export const SET_USER: string = "SET_USER";
export const setUser: (payload: IUser) => IPayloadAction<IUser> = payloadActionCreator(SET_USER);

export const SIGN_IN: string = "SIGN_IN";
export const signIn: (credentials: ICredentials, actions: FormikActions<ICredentials>) => IPayloadAction<ICredentials> & IFormAction<ICredentials> = (credentials, actions) => ({
  type: SIGN_IN,

  actions,
  payload: credentials
});

export const SIGN_OUT: string = "SIGN_OUT";
export const signOut: ActionCreator = actionCreator(SIGN_OUT);
