import {Maybe} from "monet";
import {Reducer} from "redux";

import {mkDataReducer} from "../../reducers/mkDataReducer";
import {SET_USER, SIGN_OUT} from "./actions";
import {IUser} from "./IUser";

export const user: Reducer<Maybe<IUser>> = mkDataReducer<IUser>([SET_USER], [SIGN_OUT]);
