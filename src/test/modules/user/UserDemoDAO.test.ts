import {UserDemoDAO} from "modules/user/UserDemoDAO";


const dao: UserDemoDAO = new UserDemoDAO();

test("UserDemoDAO.signIn passes the demo user credentials", () => dao
  .signIn({ email: "test@test.pl", password: "Password1" })
  .then((either) => {
    expect(either.right().email).toBe("test@test.pl");
  })
);

test("UserDemoDAO.signIn rejects the demo user given an incorrect password", () => dao
  .signIn({ email: "test@test.pl", password: "Password2" })
  .then((either) => {
    expect(either.isLeft()).toBe(true);
  })
);

test("UserDemoDAO.signIn rejects some other user", () => dao
  .signIn({ email: "test1@test.pl", password: "Password1" })
  .then((either) => {
    expect(either.isLeft()).toBe(true);
  })
);
