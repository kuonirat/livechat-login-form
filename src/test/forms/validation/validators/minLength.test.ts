import {minLength} from "forms/validation/validators/minLength";

test("minLength validator passes long enough strings", () => {
  expect(minLength(5)("foobar").isSuccess()).toBe(true);
  expect(minLength(6)("foobarfoo").isSuccess()).toBe(true);
});

test("minLength validator passes long enough collections", () => {
  expect(minLength(5)(new Array(5)).isSuccess()).toBe(true);
  expect(minLength(6)(new Array(9)).isSuccess()).toBe(true);
});

test("minLength validator fails on too short strings", () => {
  expect(minLength(7)("foobar").isFail()).toBe(true);
  expect(minLength(16)("foobarfoo").isFail()).toBe(true);
});

test("minLength validator fails on too short collections", () => {
  expect(minLength(7)(new Array(6)).isFail()).toBe(true);
  expect(minLength(16)(new Array(9)).isFail()).toBe(true);
});
