import {email} from "forms/validation/validators/email";


test("email validator passes correct e-mails", () => {
  expect(email("test@test.pl").isSuccess()).toBe(true);
  expect(email("example@example.com").isSuccess()).toBe(true);
});

test("email validator fails on incorrect e-mails", () => {
  expect(email("example").isFail()).toBe(true);
  expect(email("test@test").isFail()).toBe(true);
});
