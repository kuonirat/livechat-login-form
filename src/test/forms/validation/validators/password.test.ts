import {password} from "forms/validation/validators/password";

test("password validator passes correct passwords", () => {
  expect(password("Password1").isSuccess()).toBe(true);
  expect(password("j6xhBv").isSuccess()).toBe(true);
  expect(password("yAJss6").isSuccess()).toBe(true);
});

test("password validator fails on too short passwords", () => {
  expect(password("j6xhB").isFail()).toBe(true);
});

test("password validator fails on passwords without a lowercase letter", () => {
  expect(password("J6XHBV").isFail()).toBe(true);
});

test("password validator fails on passwords without an uppercase letter", () => {
  expect(password("j6xhbv").isFail()).toBe(true);
});

test("password validator fails on passwords without a digit", () => {
  expect(password("jsxhbv").isFail()).toBe(true);
});
