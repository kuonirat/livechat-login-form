import {required} from "forms/validation/validators/required";

test("required validator passes non-empty values", () => {
  expect(required("a").isSuccess()).toBe(true);
  expect(required(0).isSuccess()).toBe(true);
  expect(required(1).isSuccess()).toBe(true);
});

test("required validator fails on empty values", () => {
  expect(required("").isFail()).toBe(true);
  expect(required(null).isFail()).toBe(true);
  expect(required().isFail()).toBe(true);
});
