import {extractErrors} from "forms/validation/extractErrors";
import {Validation} from "monet";


test("extractErrors creates an object with extracted error messages", () => {

  expect(extractErrors({
    badNumber: Validation.Fail("bad number"),
    okNumber: Validation.Success(16),

    badString: Validation.Fail("bad string"),
    okString: Validation.Success("foo")
  })).toEqual({
    badNumber: "bad number",
    badString: "bad string"
  });
});
