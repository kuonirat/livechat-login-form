import {Validation} from "monet";

import {minLength} from "./minLength";
import {regex} from "./regex";
import {required} from "./required";

export const password: (str?: string) => Validation<string, string> = (str) => required(str)
  .flatMap(minLength(6))
  .flatMap((p) => regex(/[a-zżółćęśląźń]/)(p).failMap(() => "password must contain at least 1 lowercase letter"))
  .flatMap((p) => regex(/[A-ZŻÓŁĆĘŚLĄŹŃ]/)(p).failMap(() => "password must contain at least 1 uppercase letter"))
  .flatMap((p) => regex(/[0-9]/)(p).failMap(() => "password must contain at least 1 digit"))
;
