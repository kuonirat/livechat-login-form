import {Validation} from "monet";

import {regex} from "./regex";
import {required} from "./required";

// compliant with RFC 2822 (case insensitive)
const emailRegExp: RegExp = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;

export const email: (str?: string) => Validation<string, string> = (str) => required(str)
  .flatMap((s) => regex(emailRegExp)(s).failMap(() => "invalid email"));
