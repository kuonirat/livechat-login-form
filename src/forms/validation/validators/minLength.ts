import {Validation} from "monet";

export const minLength: (min: number) => <E, C extends ArrayLike<E>>(xs: C) => Validation<string, C> = (min) => (xs) => {

  if (xs.length < min) {
    return Validation.fail(`length should be at least ${min}`);
  } else {
    return Validation.Success(xs);
  }
};
