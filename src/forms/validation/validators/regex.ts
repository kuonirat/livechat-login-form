import {Validation} from "monet";

export const regex: (r: RegExp) => (str: string) => Validation<string, string> = (r) => (str) => {

  if (r.test(str)) {
    return Validation.Success(str);
  } else {
    return Validation.Fail("the input string doesn't match the pattern");
  }
};
