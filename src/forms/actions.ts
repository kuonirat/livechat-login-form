import {FormikActions} from "formik";

export interface IFormAction<D> {
  actions: FormikActions<Partial<D>>;
}
