import "../less/index.less";

import {LoginPage} from "components/pages/LoginPage";
import React from "react";
import {render} from "react-dom";
import {Provider} from "react-redux";
import {configureStore} from "store";

render(
  <Provider store={configureStore()}>
    <LoginPage />
  </Provider>,
  document.querySelector("#application-root")
);
