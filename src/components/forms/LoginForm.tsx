import {Field, FieldProps, Form, Formik, FormikActions} from "formik";
import {extractErrors} from "forms/validation/extractErrors";
import {email} from "forms/validation/validators/email";
import {password} from "forms/validation/validators/password";
import {signIn as signInAction} from "modules/user/actions";
import {ICredentials} from "modules/user/ICredentials";
import isEmpty from "ramda/es/isEmpty";
import React from "react";
import {connect} from "react-redux";
import {Dispatch} from "redux";

import {CheckboxInputField} from "./controls/CheckboxInputField";
import {TextInputField} from "./controls/TextInputField";

interface ILoginFormDataShape {
  email: string;
  password: string;
  remember: boolean;
}

interface IDispatchProps {
  signIn(credentials: ICredentials, actions: FormikActions<Partial<ILoginFormDataShape>>): void;
}

interface IProps extends IDispatchProps {
}

const initialValues: ILoginFormDataShape = {
  email: "",
  password: "",
  remember: false
};

class LoginFormComponent extends React.Component<IProps, {}> {

  public render(): JSX.Element {

    return (
      <Formik
        initialValues={initialValues}
        onSubmit={(values, actions) => this.props.signIn(
          {
            email: values.email as string,
            password: values.password as string
          },
          actions
        )}
        validate={(values: Partial<ILoginFormDataShape>) => extractErrors({
          email: email(values.email),
          password: password(values.password)
        })}
      >{({isSubmitting, errors}) => (
        <Form className="form form-default">
          <fieldset>
            <Field name="email">{(field: FieldProps<ICredentials>) => <TextInputField type="email" label="e-mail" {...field} />}</Field>
            <Field name="password">{(field: FieldProps<ICredentials>) => <TextInputField type="password" label="password" {...field} />}</Field>
            <div className="btns-group">
              <input className="btn" type="submit" value="log in" disabled={isSubmitting || !isEmpty(errors)} />
            </div>
            <Field name="remember">{(field: FieldProps<ICredentials>) => <CheckboxInputField label="remember me" {...field} />}</Field>
          </fieldset>
        </Form>
      )}</Formik>
    );
  }
}

export const LoginForm: React.ComponentClass<{}> = connect<{}, IDispatchProps>(
  () => ({}),
  (dispatch: Dispatch): IDispatchProps => ({
    signIn(credentials: ICredentials, actions: FormikActions<ILoginFormDataShape>): void {
      dispatch(signInAction(credentials, actions));
    }
  })
)(LoginFormComponent);
