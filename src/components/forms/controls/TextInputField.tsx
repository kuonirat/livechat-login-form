import {ErrorMessage, FieldProps} from "formik";
import * as React from "react";

interface IProps extends FieldProps<any> {
  label: string;
  type: "text" | "password" | "email";
}

export class TextInputField extends React.Component<IProps, {}> {

  public render(): JSX.Element {
    return (
      <div className="form-control">
        <label htmlFor={this.props.field.name}>{this.props.label}</label>
        <input id={this.props.field.name} type={this.props.type} {...this.props.field} />
        <ErrorMessage name={this.props.field.name}>
          {(message) => (
            <div className="form-control-error">
              {message}
            </div>
          )}
        </ErrorMessage>
      </div>
    );
  }
}
