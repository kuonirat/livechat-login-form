import {FieldProps} from "formik";
import * as React from "react";

interface IProps extends FieldProps<any> {
  label: string;
}

export class CheckboxInputField extends React.Component<IProps, {}> {

  public render(): JSX.Element {
    return (
      <div className="form-control form-control-checkbox">
        <input id={this.props.field.name} type="checkbox" {...this.props.field} />
        <label htmlFor={this.props.field.name}>{this.props.label}</label>
      </div>
    );
  }
}
