import {Flash} from "components/Flash";
import {LoginForm} from "components/forms/LoginForm";
import {LogOutButton} from "components/LogOutButton";
import {IUser} from "modules/user/IUser";
import {Maybe} from "monet";
import React from "react";
import {connect} from "react-redux";
import {IMainState} from "state";

interface IStateProps {
  user: Maybe<IUser>;
}

interface IProps extends IStateProps {}

class LoginPageComponent extends React.Component<IProps, {}> {

  public render(): JSX.Element {
    return (
      <div className="login-page">
        <div>
          <div className="flash-container">
            <Flash />
          </div>
          <div className="content-box">
            {this.props.user.cata(
              () => <LoginForm />,
              (user) => <>
                <p>hi, {user.firstName}!</p>
                <div className="btns-group">
                  <LogOutButton />
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export const LoginPage: React.ComponentClass<{}> = connect<IProps>(
  (state: IMainState): IStateProps => ({ user: state.user })
)(LoginPageComponent);
