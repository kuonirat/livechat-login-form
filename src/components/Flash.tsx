import {IFlash} from "modules/flash/IFlash";
import {Maybe} from "monet";
import React from "react";
import {connect} from "react-redux";
import {IMainState} from "state";

import {OrNothing} from "./OrNothing";

interface IStateProps {
  flash: Maybe<IFlash>;
}

interface IProps extends IStateProps {}

class FlashComponent extends React.Component<IProps, {}> {

  public render(): JSX.Element {
    return <OrNothing maybe={this.props.flash}>{(flash) =>
      <div className={`flash flash-${flash.level}`}>{flash.message}</div>
    }</OrNothing>;
  }
}

export const Flash: React.ComponentClass<{}> = connect<IProps>(
  (state: IMainState): IStateProps => ({ flash: state.flash })
)(FlashComponent);
