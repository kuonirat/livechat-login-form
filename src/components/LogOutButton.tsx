import {signOut} from "modules/user/actions";
import React from "react";
import {connect} from "react-redux";
import {Dispatch} from "redux";

interface IDispatchProps {
  logOut(): void;
}

interface IProps extends IDispatchProps {}

class LogOutButtonComponent extends React.Component<IProps, {}> {

  public render(): JSX.Element {
    return (
      <button className="btn" onClick={() => this.props.logOut()}>log out</button>
    );
  }
}

export const LogOutButton: React.ComponentClass<{}> = connect<{}, IDispatchProps>(
  () => ({}),
  (dispatch: Dispatch): IDispatchProps => ({
    logOut(): void {
      dispatch(signOut());
    }
  })
)(LogOutButtonComponent);
