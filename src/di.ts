import {IUserDAO} from "modules/user/IUserDAO";
import {UserDemoDAO} from "modules/user/UserDemoDAO";

export interface IInjector {
  userDAO: IUserDAO;
}

export const injector: IInjector = {
  userDAO: new UserDemoDAO()
};
