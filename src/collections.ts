import {Maybe} from "monet";

export const findSafe: <T>(a: T[], pred: (o: T) => boolean) => Maybe<T> = <T>(a: T[], pred: (o: T) => boolean) =>
  Maybe.fromNull(a.find(pred));
