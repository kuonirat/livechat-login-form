import {IFlash} from "modules/flash/IFlash";
import {IUser} from "modules/user/IUser";
import {Maybe} from "monet";

export interface IMainState {
  flash: Maybe<IFlash>;
  user: Maybe<IUser>;
}
